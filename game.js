/**
 * Created by ashishtilara on 18/7/17.
 */
class Game {
  constructor(width, height) {
    this.width = width;
    this.height = height;
    this.board = this.buildBoard(width, height);
  }

  evolve() {
    let board = this.buildBoard(this.width, this.height);
    let neighbours = [];
    // console.log("currentBoard", board);
    for (let x = 0; x < this.width; x++) {
      for (let y = 0; y < this.height; y++) {
        neighbours = this.fetchNeighbours(this.board, x, y);
        let alive = neighbours.filter(v => v===1).length;
        let dead = neighbours.filter(v => v===0).length;

        if (alive < 2 || alive > 3) {
          board[x][y] = 0;
        } else if ([2, 3].indexOf(alive) !== -1 || dead === 3) {
          board[x][y] = 1;
        }
      }
    }
    this.render(board);
    return board;
  }

  fetchNeighbours(board, x, y) {
    let neighbours = [];
    for (let i = x-1; i <= x+1; i++) {
      for (let j = y-1; j <= y+1; j++) {
        if (!((i<0 || i>=this.width) || (j <0 || j>=this.height) || (i===x && j===y))) {
          neighbours.push(board[i][j]);
        }
      }
    }
    return neighbours;
  }

  render(board) {
    for (let i = 0; i < this.width; i++) {
      let str = "";
      for (let j = 0; j < this.height; j++) {
        str += board[i][j];
      }
      console.log(str);
    }
  }

  setState(board) {
    this.board = board;
  }

  buildBoard(width, height) {
    let board = new Array(width);
    for (let i = 0; i < width; i++) {
      board[i] = new Array(height);
      for (let j = 0; j < height; j++) {
        board[i][j] = 0;
      }
    }
    return board;
  }
}


module.exports = Game;