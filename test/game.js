/**
 * Created by ashishtilara on 18/7/17.
 */
const chai = require('chai');
const Game = require('../game');
chai.should();

describe('game of life', () => {
  it('should hold the game board', () => {
    let game = new Game(2,2);
    game.board.length.should.be.eql(2);
    game.board[0].length.should.be.eql(2);
    game.board[1].length.should.be.eql(2);
  });

  describe('should evolve to the next generation', () => {
    let game;

    beforeEach(() => {
      game = new Game(5, 5);
    });

    it('should die of underpopulation', () => {
      let state = [
        [0,0,0,0,0],
        [0,0,0,0,0],
        [0,1,1,1,0],
        [0,0,0,0,0],
        [0,0,0,0,0]
      ];
      game.setState(state);
      game.evolve()[2][1].should.be.eql(0);
      game.evolve()[2][4].should.be.eql(0);
    });

    it('should live through survive', () => {
      let state = [
        [0,0,0,0,0],
        [0,0,0,0,0],
        [0,1,1,1,0],
        [0,0,0,0,0],
        [0,0,0,0,0]
      ];
      game.setState(state);
      game.evolve()[2][2].should.be.eql(1);
    });

    it('should die of overcrowding', () => {
      let state = [
        [0,0,0,0,0],
        [0,1,1,0,0],
        [0,1,1,1,0],
        [0,0,0,0,0],
        [0,0,0,0,0]
      ];
      game.setState(state);
      game.evolve()[1][2].should.be.eql(0);
    });

    it('should live through reproduction', () => {
      let state = [
        [0,0,0,0,0],
        [0,0,0,0,0],
        [0,1,1,1,0],
        [0,0,0,0,0],
        [0,0,0,0,0]
      ];
      game.setState(state);
      game.evolve()[1][2].should.be.eql(1);
    });
  });
});