Feature: Asset depreciation
  As a user of Cucumber.js
  I want to have documentation on Cucumber
  So that I can concentrate on building awesome applications

  Scenario: Straight line after 1 year
    Given I have an asset costing $5000
    And I am using Straight value depreciation
    When I calculate the value after 1 year
    Then the remaining value should be $4000