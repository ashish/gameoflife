/**
 * Created by ashishtilara on 18/7/17.
 */
var {defineSupportCode} = require('cucumber');
var chai = require('chai');
chai.should();
class Asset {
  constructor(initialValue, age) {
    this.initialValue = initialValue;
    this.age = age;
  }

  calculate(depreciation) {
    this.remainingValue = this.initialValue- depreciation.calculate(this.initialValue, this.age);
  }
}

class StraightDepreciation {
  constructor(year) {
    this.year = year;
  }

  setYear(year) {
    this.year = year;
  }

  calculate(initialValue, age) {
    let depreciation;
    depreciation = this.year * (initialValue/age);
    return depreciation;
  }
}

defineSupportCode(function({Given, When, Then}) {
  let asset, straightDepreciation;
  Given('I have an asset costing $5000', function() {
    return asset = new Asset(5000, 0, 5);
  });

  Given('I am using Straight value depreciation', function () {
    return straightDepreciation = new StraightDepreciation();
  });

  When('I calculate the value after 1 year', function () {
    return straightDepreciation.setYear(1);
  });

  Then('the remaining value should be $4000', function () {
    asset.calculate(straightDepreciation);
    asset.remainingValue.should.be.eql(4000);
  });
});